package com.umbrellait.android.pdp.rx_base.second_screen

import android.app.Dialog
import android.graphics.Color
import android.graphics.drawable.ColorDrawable
import android.os.Bundle
import android.util.Log
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.*
import androidx.activity.OnBackPressedCallback
import androidx.core.os.bundleOf
import androidx.core.widget.addTextChangedListener
import androidx.fragment.app.Fragment
import com.umbrellait.android.pdp.rx_base.R
import com.umbrellait.android.pdp.rx_base.data.search.Balance
import com.umbrellait.android.pdp.rx_base.data.search.MainRepository
import com.umbrellait.android.pdp.rx_base.databinding.FragmentSecondBinding
import com.umbrellait.android.pdp.rx_base.first_screen.FirstFragment


class SecondFragment : Fragment(), SecondFragmentContract.View {

    private lateinit var binding: FragmentSecondBinding
    private lateinit var dialog: Dialog
    private val name: String by lazy {
        arguments?.getString(KEY_NAME).orEmpty()
    }

    private val presenter by lazy {
        SecondPresenter(this)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        dialog = Dialog(requireContext())
        showDialog()
        binding.etSecondName.setText(name, TextView.BufferType.EDITABLE)
        onBackPressed()
        binding.buttonBuy.isEnabled = false


        binding.etAccountNumber.addTextChangedListener {
            presenter.validateAccountNumber(binding.etAccountNumber.text.toString(), 0)
            presenter.validateInput(binding.etAccountNumber.text.toString(), 0)
        }

        binding.etTypeOfPayment.addTextChangedListener {
            presenter.validateInput(binding.etTypeOfPayment.text.toString(), 1)
        }

        binding.etAmout.addTextChangedListener {
            try {
                presenter.validateAmount(
                    binding.etAmout.text.toString().toBigDecimal(),
                    2
                )
            } catch (e: NumberFormatException) {
                presenter.updateButtonSubject(firstFlag = true, secondFlag = false)
            }
            presenter.validateInput(binding.etAmout.text.toString(), 2)
        }


        binding.button1.setOnClickListener {
            binding.etAccountNumber.text.clear()
            presenter.updateView(0, false)
            presenter.updateButtonSubject(firstFlag = false, secondFlag = false)
        }

        binding.button2.setOnClickListener {
            binding.etTypeOfPayment.text.clear()
            presenter.updateView(1, false)
            presenter.updateButtonSubject(firstFlag = true, secondFlag = false)
        }

        binding.button3.setOnClickListener {
            binding.etAmout.text.clear()
            presenter.updateView(2, false)
            presenter.updateButtonSubject(firstFlag = false, secondFlag = false)
        }

        binding.buttonBuy.setOnClickListener {
            presenter.buy()
        }
    }

    override fun onCreateView(
        inflater: LayoutInflater, container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentSecondBinding.inflate(inflater, container, false)
        return binding.root
    }

    companion object {
        private const val KEY_NAME = "KEY-NAME"
        fun newInstance(name: String): SecondFragment {
            return SecondFragment().apply {
                arguments = bundleOf(KEY_NAME to name)
            }
        }
    }


    override fun onSuccess() {
        Toast.makeText(requireContext(), "buy", Toast.LENGTH_SHORT).show()
    }

    override fun onError(message: String) {
        Toast.makeText(requireContext(), message, Toast.LENGTH_SHORT).show()
    }

    override fun onDestroy() {
        super.onDestroy()
        presenter.disposeAll()
    }

    private fun onBackPressed() {
        requireActivity().onBackPressedDispatcher.addCallback(
            this,
            object : OnBackPressedCallback(true) {
                override fun handleOnBackPressed() {
                    requireActivity().supportFragmentManager.beginTransaction()
                        .replace(R.id.container, FirstFragment.newInstance())
                        .commit()
                }
            })
    }

    private fun showDialog() {
        dialog.setContentView(R.layout.custom_dialog)
        val close = dialog.findViewById<ImageView>(R.id.imgClose)
        close.setOnClickListener {
            dialog.dismiss()
        }

        val ok = dialog.findViewById<Button>(R.id.ok)
        val etLower = dialog.findViewById<EditText>(R.id.etLower)
        val etUpper = dialog.findViewById<EditText>(R.id.etUpper)

        ok.setOnClickListener {
            MainRepository().returnLimits(
                etLower.text.toString().toBigDecimal(),
                etUpper.text.toString().toBigDecimal()
            )
            binding.etAmout.hint =
                "Ваш средний баланс: ${(Balance.lower + Balance.upper) / "2".toBigDecimal()}"

            dialog.dismiss()
        }
        dialog.window?.setBackgroundDrawable(ColorDrawable(Color.TRANSPARENT))
        dialog.show()
    }

    override fun setViewCancelVisible(position: Int) {
        val arrayOfButton = arrayOf(binding.button1, binding.button2, binding.button3)
        arrayOfButton[position].visibility = View.VISIBLE
        presenter.disposeCancelView()
    }

    override fun setViewCancelInvisible(position: Int) {
        val arrayOfButton = arrayOf(binding.button1, binding.button2, binding.button3)
        arrayOfButton[position].visibility = View.INVISIBLE
        presenter.disposeCancelView()
    }

    override fun setViewWarningInvisible(position: Int) {
        val arrayOfTextView =
            arrayOf(binding.etNotValidOne, binding.etNotValidTwo, binding.etNotValidThree)
        arrayOfTextView[position].visibility = View.INVISIBLE
    }

    override fun setViewWarningVisible(position: Int) {
        val arrayOfTextView =
            arrayOf(binding.etNotValidOne, binding.etNotValidTwo, binding.etNotValidThree)
        arrayOfTextView[position].visibility = View.VISIBLE
    }

    override fun setClickableButton() {
        binding.buttonBuy.isEnabled = true
    }

    override fun setNotClickableButton() {
        binding.buttonBuy.isEnabled = false
    }
}