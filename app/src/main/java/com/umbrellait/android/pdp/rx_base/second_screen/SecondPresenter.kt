package com.umbrellait.android.pdp.rx_base.second_screen

import android.util.Log
import com.umbrellait.android.pdp.rx_base.data.search.MainRepository
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo
import io.reactivex.rxkotlin.subscribeBy
import io.reactivex.subjects.BehaviorSubject
import java.math.BigDecimal

class SecondPresenter(private val view: SecondFragmentContract.View) :
    SecondFragmentContract.Presenter {

    private val compositeDisposable = CompositeDisposable()
    private val viewDisposable = CompositeDisposable()
    private val subjectView = BehaviorSubject.create<Boolean>()
    private val enabledButtonSubject = BehaviorSubject.create<Pair<Boolean, Boolean>>()

    private val repository = MainRepository()

    init {
        setClickableButton()
    }

    override fun updateButtonSubject(firstFlag: Boolean, secondFlag: Boolean) {
        enabledButtonSubject.onNext(Pair(firstFlag, secondFlag))
    }

    override fun validateAmount(amount: BigDecimal, position: Int) {
        repository.validateAmount(amount)
            .subscribeBy(
                onSuccess = {
                    if (it) {
                        view.setViewWarningInvisible(position)
                        enabledButtonSubject.onNext(Pair(true, true))
                    } else {
                        view.setViewWarningVisible(position)
                        enabledButtonSubject.onNext(Pair(false, false))
                    }
                }
            ).addTo(compositeDisposable)
    }

    override fun setClickableButton() {
        enabledButtonSubject.subscribe {
            if (it.first && it.second) {
                view.setClickableButton()
            } else {
                view.setNotClickableButton()
            }
        }.addTo(compositeDisposable)
    }

    override fun buy() {
        view.onSuccess()
    }

    override fun validateAccountNumber(accountNumber: String, position: Int) {
        repository.validateAccountNumber(accountNumber)
            .subscribeBy(
                onSuccess = {
                    if (it) {
                        enabledButtonSubject.onNext(Pair(true, false))
                        view.setViewWarningInvisible(position)
                    } else {
                        enabledButtonSubject.onNext(Pair(false, false))
                        view.setViewWarningVisible(position)
                    }
                },
                onError = {
                    Log.i("TAG", "error ${it.localizedMessage}")
                }
            ).addTo(compositeDisposable)
    }

    override fun validateInput(text: String, position: Int) {
        if (text.isEmpty()) {
            updateView(position, false)
        } else {
            updateView(position, true)
        }
    }

    override fun updateView(position: Int, flag: Boolean) {
        subjectView.onNext(flag)
        subjectView
            .subscribeBy(
                onNext = {
                    if (it) {
                        view.setViewCancelVisible(position)
                    } else {
                        view.setViewCancelInvisible(position)
                    }
                }
            ).addTo(viewDisposable)

    }

    fun disposeAll() {
        compositeDisposable.dispose()
        viewDisposable.dispose()
    }

    fun disposeCancelView() {
        viewDisposable.dispose()
    }
}