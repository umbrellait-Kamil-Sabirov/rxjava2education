package com.umbrellait.android.pdp.rx_base.first_screen

import android.util.Log
import androidx.appcompat.widget.SearchView
import io.reactivex.ObservableEmitter
import io.reactivex.ObservableOnSubscribe

class RxSearchObservable(private val editText: SearchView) : ObservableOnSubscribe<String> {
    override fun subscribe(emitter: ObservableEmitter<String>) {
        editText.setOnQueryTextListener(object : SearchView.OnQueryTextListener {
            override fun onQueryTextSubmit(query: String?): Boolean {
                emitter.onComplete()
                Log.i("TAG","onComplete")
                emitter.setCancellable {
                     if (!emitter.isDisposed){
                         editText.setOnQueryTextListener(null)
                     }
                }
                return true
            }

            override fun onQueryTextChange(newText: String): Boolean {
                emitter.onNext(newText)
                Log.i("TAG","onNext")
                return true
            }
        })

    }
}