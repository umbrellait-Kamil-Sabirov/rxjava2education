package com.umbrellait.android.pdp.rx_base.first_screen

import android.view.LayoutInflater
import android.view.ViewGroup
import androidx.recyclerview.widget.RecyclerView
import com.umbrellait.android.pdp.rx_base.databinding.SearchItemBinding
import java.util.ArrayList

class SearchAdapter(private val clickItem: ClickItem) :
    RecyclerView.Adapter<SearchAdapter.SearchViewHolder>() {

    private val listOfNames = ArrayList<String>()

    fun update(new: List<String>) {
        listOfNames.clear()
        listOfNames.addAll(new)
        notifyDataSetChanged()
    }

    inner class SearchViewHolder(
        private val binding: SearchItemBinding,
        private val clickItem: ClickItem
    ) :
        RecyclerView.ViewHolder(binding.root) {
        fun bind(name: String) {
            binding.name.text = name
            binding.name.setOnClickListener {
                clickItem.click(name)
            }
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): SearchViewHolder {
        val layoutInflater = LayoutInflater.from(parent.context)
        val binding = SearchItemBinding.inflate(layoutInflater, parent, false)
        return SearchViewHolder(binding, clickItem)
    }

    override fun onBindViewHolder(holder: SearchViewHolder, position: Int) {
        holder.bind(listOfNames[position])
    }

    override fun getItemCount(): Int = listOfNames.size


    fun remove(position: Int) {
        listOfNames.removeAt(position)
        notifyItemRemoved(position)
    }

}