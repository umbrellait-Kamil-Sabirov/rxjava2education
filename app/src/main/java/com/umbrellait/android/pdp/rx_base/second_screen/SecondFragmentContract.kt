package com.umbrellait.android.pdp.rx_base.second_screen

import java.math.BigDecimal


interface SecondFragmentContract {

    interface View {
        fun onSuccess()
        fun onError(message: String)
        fun setViewCancelVisible(position: Int)
        fun setViewCancelInvisible(position: Int)
        fun setViewWarningVisible(position: Int)
        fun setViewWarningInvisible(position: Int)
        fun setClickableButton()
        fun setNotClickableButton()
    }

    interface Presenter {
        fun buy()
        fun updateButtonSubject(firstFlag: Boolean, secondFlag: Boolean)
        fun setClickableButton()
        fun validateAccountNumber(accountNumber: String, position: Int)
        fun updateView(position: Int, flag: Boolean)
        fun validateInput(text: String, position: Int)
        fun validateAmount(amount: BigDecimal, position: Int)
    }

}