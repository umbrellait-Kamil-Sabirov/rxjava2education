package com.umbrellait.android.pdp.rx_base

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log
import androidx.activity.OnBackPressedCallback
import com.umbrellait.android.pdp.rx_base.R
import com.umbrellait.android.pdp.rx_base.databinding.ActivityMainBinding
import com.umbrellait.android.pdp.rx_base.first_screen.FirstFragment

class MainActivity : AppCompatActivity() {
    private lateinit var binding: ActivityMainBinding
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        binding = ActivityMainBinding.inflate(layoutInflater)
        setContentView(binding.root)
        supportFragmentManager.beginTransaction()
            .replace(R.id.container, FirstFragment())
            .commit()
    }

}