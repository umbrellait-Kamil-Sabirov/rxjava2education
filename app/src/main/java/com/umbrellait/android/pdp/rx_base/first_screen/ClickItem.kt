package com.umbrellait.android.pdp.rx_base.first_screen

interface ClickItem {

    fun click(name: String)

}