package com.umbrellait.android.pdp.rx_base.first_screen

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.widget.Toast
import androidx.fragment.app.Fragment
import androidx.recyclerview.widget.ItemTouchHelper
import androidx.recyclerview.widget.RecyclerView
import com.umbrellait.android.pdp.rx_base.R
import com.umbrellait.android.pdp.rx_base.databinding.FragmentFirstBinding
import com.umbrellait.android.pdp.rx_base.second_screen.SecondFragment


class FirstFragment : Fragment(), ClickItem, FirstFragmentContract.View {

    private lateinit var binding: FragmentFirstBinding
    private val presenter by lazy {
        FirstPresenter(this)
    }

    private val adapter = SearchAdapter(this)
    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.recyclerView.adapter = adapter
        deleteSwipedItem()
        presenter.updateData(binding.searchEditText)
    }

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ): View {
        binding = FragmentFirstBinding.inflate(inflater, container, false)
        return binding.root
    }

    override fun click(name: String) {
        requireActivity().supportFragmentManager.beginTransaction()
            .replace(R.id.container, SecondFragment.newInstance(name))
            .commit()
    }

    private fun deleteSwipedItem() {
        val swipeCallback = object : SwipeToDeleteCallback(requireContext()) {
            override fun onSwiped(viewHolder: RecyclerView.ViewHolder, direction: Int) {
                val position = viewHolder.adapterPosition
                adapter.remove(position)
            }
        }
        val itemTouchHelper = ItemTouchHelper(swipeCallback)
        itemTouchHelper.attachToRecyclerView(binding.recyclerView)
    }

    override fun onError(text: String) {
        Toast.makeText(requireContext(), text, Toast.LENGTH_LONG).show()
    }

    override fun showList(listOfNames: List<String>) {
        adapter.update(listOfNames)
    }

    override fun showSizeOfList(size: Int) {
        binding.textResult.text = "Кол-во найденных элементов $size"
    }


    override fun onDestroy() {
        super.onDestroy()
        presenter.disposeAll()
    }

    companion object {
        fun newInstance(): FirstFragment {
            return FirstFragment()
        }
    }
}