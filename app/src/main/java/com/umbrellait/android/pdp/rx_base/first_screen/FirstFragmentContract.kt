package com.umbrellait.android.pdp.rx_base.first_screen


import androidx.appcompat.widget.SearchView

interface FirstFragmentContract {

    interface View{

        fun onError(text: String)
        fun showList(listOfNames: List<String>)
        fun showSizeOfList(size: Int)
    }

    interface Presenter{
        fun updateData(searchView: SearchView)
    }
}