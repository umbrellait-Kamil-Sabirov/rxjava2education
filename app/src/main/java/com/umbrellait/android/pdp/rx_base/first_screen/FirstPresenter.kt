package com.umbrellait.android.pdp.rx_base.first_screen

import androidx.appcompat.widget.SearchView
import com.umbrellait.android.pdp.rx_base.data.search.MainRepository
import io.reactivex.Observable
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.rxkotlin.addTo

class FirstPresenter(private val view: FirstFragmentContract.View) :
    FirstFragmentContract.Presenter {

    private val compositeDisposable = CompositeDisposable()

    override fun updateData(searchView: SearchView) {
        val searchObservable = Observable.create(RxSearchObservable(searchView))

        searchObservable.concatMapSingle { name ->
            MainRepository().search(name)
        }.doOnError { error ->
            view.onError(error.localizedMessage ?: "unknown error")
        }.subscribe { list ->
            view.showList(list)
            view.showSizeOfList(list.size)
        }.addTo(compositeDisposable)
    }


    fun disposeAll() {
        compositeDisposable.dispose()
    }
}
