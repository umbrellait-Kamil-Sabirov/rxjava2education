package com.umbrellait.android.pdp.rx_base.data.search

import java.math.BigDecimal

object Balance {
    var upper: BigDecimal = BigDecimal.ZERO
    var lower: BigDecimal = BigDecimal.ZERO
}