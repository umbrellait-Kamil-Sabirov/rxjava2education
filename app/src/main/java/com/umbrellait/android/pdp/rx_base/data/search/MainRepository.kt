package com.umbrellait.android.pdp.rx_base.data.search

import io.reactivex.Single
import java.math.BigDecimal

class MainRepository {
    fun search(query: String): Single<List<String>> {
        return if (query.isEmpty()) {
            Single.just(listOf())
        } else {
            retrieveItemsOrError().map { items ->
                items.filter { item ->
                    item.contains(query)
                }.sorted()
            }
        }
    }


    fun validateAccountNumber(num: String): Single<Boolean> {
        return if (num.length < 8) {
            Single.just(false)
        } else {
            Single.just(
                (num[5] == '6' && num[6] == '4' && num[7] == '3') ||
                        (num[5] == '8' && num[6] == '1' && num[7] == '0')
            )
        }
    }

    fun validateAmount(price: BigDecimal): Single<Boolean> {
        return returnLimits(Balance.lower, Balance.upper)
            .map { limit ->
                (price <= limit.upper && price >= limit.lower) ||
                        (price >= limit.upper && price <= limit.lower)
            }


    }


    fun returnLimits(lower: BigDecimal, upper: BigDecimal): Single<PaymentLimits> {
        Balance.lower = lower
        Balance.upper = upper
        return Single.just(PaymentLimits(lower, upper))
    }


    private fun retrieveItemsOrError(): Single<MutableList<String>> {
        return Single.just(items)
    }

    companion object {
        val items = mutableListOf(
            "Иванов",
            "Васильев",
            "Петров",
            "Смирнов",
            "Михайлов",
            "Фёдоров",
            "Соколов",
            "Яковлев",
            "Попов",
            "Андреев",
            "Алексеев",
            "Александров",
            "Лебедев",
            "Григорьев",
            "Степанов",
            "Семёнов",
            "Павлов",
            "Богданов",
            "Николаев",
            "Дмитриев",
            "Егоров",
            "Волков",
            "Кузнецов",
            "Никитин",
            "Иванов",
            "Соловьёв",
            "Тимофеев",
            "Орлов",
            "Афанасьев",
            "Филиппов",
            "Сергеев",
            "Захаров",
            "Матвеев",
            "Виноградов",
            "Кузьмин",
            "Максимов",
            "Козлов",
            "Ильин",
            "Герасимов",
            "Марков",
            "Новиков",
            "Морозов",
            "Романов",
            "Осипов",
            "Макаров",
            "Зайцев",
            "Беляев",
            "Гаврилов",
            "Антонов",
            "Ефимов",
            "Леонтьев",
            "Давыдов",
            "Гусев",
            "Данилов",
            "Киселёв",
            "Иванов",
            "Сорокин",
            "Тихомиров",
            "Крылов",
            "Никифоров",
            "Кондратьев",
            "Кудрявцев",
            "Борисов",
            "Жуков",
            "Воробьёв",
            "Щербаков",
            "Поляков",
            "Савельев",
            "Шмидт",
            "Трофимов",
            "Чистяков",
            "Баранов",
            "Сидоров",
            "Соболев",
            "Карпов",
            "Белов",
            "Миллер",
            "Титов",
            "Львов",
            "Фролов",
            "Игнатьев",
            "Комаров",
            "Прокофьев",
            "Быков",
            "Абрамов",
            "Голубев",
            "Пономарёв",
            "Покровский",
            "Мартынов",
            "Кириллов",
            "Шульц",
            "Миронов",
            "Фомин",
            "Власов",
            "Троицкий",
            "Федотов",
            "Назаров",
            "Ушаков",
            "Денисов",
            "Константинов",
            "Воронин",
            "Наумов",
        )
    }
}

class PaymentLimits(
    val lower: BigDecimal,
    val upper: BigDecimal,
)

